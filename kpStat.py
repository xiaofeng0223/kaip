# !/usr/bin/env python
# -*- coding: utf-8 -*-

import csv
import re
import datetime
import xlwt

import pymysql


CONFIG = list()
with open("config", "r", encoding="utf-8") as f:
    CONFIG.append(f.read().split("\n"))


db_host = CONFIG[0][0].split("=")[1].replace('"', "")
db_port = CONFIG[0][1].split("=")[1].replace('"', "")
db_user = CONFIG[0][2].split("=")[1].replace('"', "")
db_pass = CONFIG[0][3].split("=")[1].replace('"', "")
class MySQL:
    # 在这里配置自己的SQL服务器
    def __init__(self, dbname):
        self.__db = None
        self.db_host = db_host
        self.db_port = int(db_port)
        self.db_user = db_user
        self.db_pass = db_pass
        self.db_database = dbname
        self.db_status = "fail"
        self.__connect()

    def __del__(self):
        if self.__db is not None:
            self.__db.close()

    def __connect(self):
        if self.__db is None:
            try:
                self.__db = pymysql.connect(
                    host=self.db_host,
                    port=self.db_port,
                    user=self.db_user,
                    passwd=self.db_pass,
                    db=self.db_database,
                    charset="utf8"
                )
            except Exception as e:
                print(e)
            self.db_status = "success" if self.__db is not None else "fail"
        return self.__db

    def query(self, _sql):
        cursor = self.__connect().cursor()
        try:
            cursor.execute(_sql)
            data = cursor.fetchall()
            # 提交到数据库执行
            self.__connect().commit()
        except Exception as e:
            print(e)
            # 如果发生错误则回滚
            self.__connect().rollback()
            return []
        return data

mysql = MySQL("gxdz")


####TODO select DISTINCT provinces from gxdzpj;  去重查询


def getNowTime():
    return "[" + str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')) + "]"


def getCsvData():
    data = list()
    with open('grafana_data_export.csv', 'r', encoding="utf-8") as f:
        reader = csv.reader(f)
        for row in reader:
            data.append(row)
    return data, len(data)


def getEveryProv(all, data):
    count = 0
    print("{}正在导入数据到数据库，请稍等....".format(getNowTime()))
    for index, da in enumerate(data):
        if index > 0:
            count += 1
            col_data = da[0].split(";")
            d_time_ = col_data[0]
            prov_ = col_data[1]
            sfkpzs_ = col_data[2]
            csjzs_ = col_data[3]
            cz_ = col_data[4]

            if sfkpzs_[-2:] == 'K"':
                sfkpzs_ = int(float(eval(re.findall(r"\d+\.\d+", sfkpzs_)[0])) * 1000)
            else:
                sfkpzs_ = int(float(eval(sfkpzs_)))
            if csjzs_[-2:] == 'K"':
                csjzs_ = int(float(eval(re.findall(r"\d+\.\d+", csjzs_)[0])) * 1000)
            else:
                csjzs_ = int(float(eval(csjzs_)))
            if cz_[-2:] == 'K"':
                cz_ = int(float(eval(re.findall(r"\d+\.\d+", cz_)[0])) * 1000)
            else:
                cz_ = int(float(eval(cz_)))
            sql = "INSERT INTO `gxdzpj`(`id`,`day_time`,`provinces`,`sfkpzs`,`csjzs`,`cz`)VALUES ({},'{}',{},{},{},{});".\
                format(count, d_time_, prov_, sfkpzs_, csjzs_, cz_)
            mysql.query(sql)
    print("{}导入数据到数据库完成，源文件有{}条数据,成功导入{}条数据，失败{}条...".format(getNowTime(), all, count, all - count))



def getFile(between_time):
    Prov = ["西藏自治区", "海南省", "河南省", "天津市", "湖南省", "四川省", "上海市", "江苏省", "黑龙江省",\
            "宁夏回族自治区", "江西省", "重庆市", "新疆维吾尔自治区", "内蒙古自治区", "陕西省", "广东省",\
            "湖北省", "山东省", "云南省", "北京市", "河北省", "广西壮族自治区", "甘肃省", "安徽省", "青海省",\
            "山西省", "浙江省", "贵州省", "吉林省", "福建省"]
    Title_list = ["日期", "省份", "省份开票总数", "传税局总数", "开票量和版式文件量的差值"]
    a_Prov = list()
    workbook = xlwt.Workbook(encoding='utf-8')
    worksheet = workbook.add_sheet('联通开票统计')
    borders = xlwt.Borders()
    borders.left = 1  # 添加边框-虚线边框
    borders.right = 1  # 添加边框-虚线边框
    borders.top = 1  # 添加边框-虚线边框
    borders.bottom = 1  # 添加边框-虚线边框
    style = xlwt.XFStyle()
    style.borders = borders
    for i in range(5):
        worksheet.col(i).width = 6000   #设置单元格宽高
        #worksheet.col(i).height = 6000   #设置单元格宽高
    for i in range(5):
        worksheet.write(0, i, Title_list[i], style)
    #workbook.save('联通开票统计.xls')
    for p in Prov:
        b_Prov = list()
        sfkpzs_sql = "SELECT SUM(sfkpzs)FROM gxdzpj where provinces='{}'".format(p)
        csjzs_sql = "SELECT SUM(csjzs)FROM gxdzpj where provinces='{}'".format(p)
        cz_sql = "SELECT SUM(cz)FROM gxdzpj where provinces='{}'".format(p)
        sfkpzs_data = mysql.query(sfkpzs_sql)[0][0]
        csjzs_data = mysql.query(csjzs_sql)[0][0]
        cz_data = mysql.query(cz_sql)[0][0]
        b_Prov.append(between_time)
        b_Prov.append(p)
        b_Prov.append(sfkpzs_data)
        b_Prov.append(csjzs_data)
        b_Prov.append(cz_data)
        a_Prov.append(b_Prov)
    count = 0
    for d in a_Prov:
        count += 1
        for j in range(5):
            worksheet.write(count, j, d[j], style)
    workbook.save('联通开票统计.xls')

def main():
    try:
        data, num = getCsvData()
        startDate = data[num - 1][0].split(";")[0]
        endDate = data[1][0].split(";")[0]
        between_time = "{}到{}".format(startDate, endDate)
        print("{}合计有{}条数据需要清洗插入数据库进行处理...".format(getNowTime(), num))
        print("{}正在入库从<{}>日期的联通各省份的开票量...".format(getNowTime(), between_time))
        mysql.query("truncate gxdzpj")
        getEveryProv(num - 1, data)
        getFile(between_time)
        print("{}联通开票统计文件在当前目录下已生成...".format(getNowTime()))
        input()
    except Exception as e:
        print(e)
        print("{}请确保导出的是原始无修改的格式...".format(getNowTime()))


if __name__ == '__main__':
    main()


