/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50726
Source Host           : localhost:3306
Source Database       : gxdz

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2021-04-14 18:23:32
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for gxdzpj
-- ----------------------------
DROP TABLE IF EXISTS `gxdzpj`;
CREATE TABLE `gxdzpj` (
  `id` int(11) NOT NULL,
  `day_time` datetime DEFAULT NULL COMMENT '开票时间',
  `provinces` varchar(20) NOT NULL COMMENT '省份',
  `sfkpzs` int(11) NOT NULL COMMENT '省份开票总数',
  `csjzs` int(11) NOT NULL COMMENT '传税局总数',
  `cz` int(5) NOT NULL COMMENT '开票量和版式文件量的差值',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
